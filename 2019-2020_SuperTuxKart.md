* 2019-2020: SuperTuxKart
    - [Wrong Icon](https://github.com/supertuxkart/stk-code/issues/4070)
    - [Brake is reverse](https://github.com/supertuxkart/stk-code/issues/3858)
    - [Icons too small](https://github.com/supertuxkart/stk-code/issues/4098)
    - [Auto-kick players](https://github.com/supertuxkart/stk-code/issues/4126)
    - [Chrono fix](https://github.com/supertuxkart/stk-code/pull/4161)
    - [Possessed addons](https://github.com/supertuxkart/stk-code/pull/4162)
    - [Message frequency](https://github.com/supertuxkart/stk-code/pull/4168)
    - [Rename config](https://github.com/supertuxkart/stk-code/pull/4233)
    - [Clickable links](https://github.com/supertuxkart/stk-code/issues/3453)
    
