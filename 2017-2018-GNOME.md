* 2017-2018: GNOME Music
    - [Master branch fails to build from source on Debian Sid.](https://bugzilla.gnome.org/show_bug.cgi?id=788692)
    - [Update README.md with new compiling instruction](https://bugzilla.gnome.org/show_bug.cgi?id=790454)
